import { HashRouter, Route, Routes } from "react-router-dom"

import routes from "./router/routes"
import Login from "./views/pages/Login"
import PrivateRoute from "./router/PrivateRoute"
import PublicRoute from "./router/PublicRoute"

//BrowserRouter to HashRouter --build

function App() {
    return (
        <HashRouter>
            <Routes>
                <Route element={<PublicRoute />}>
                    <Route path="/" element={<Login />} exact />
                    <Route path="/login" element={<Login />} />
                </Route>


                <Route element={<PrivateRoute />}>
                    {
                        routes.map(({ element, path }) => (
                            <Route
                                path={path}
                                key={path}
                                element={element}
                            />
                        ))
                    }
                </Route>
            </Routes>
        </HashRouter>
    )
}

export default App
