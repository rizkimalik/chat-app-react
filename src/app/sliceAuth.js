import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    username: '',
    email: '',
    uuid: '',
    connected: '',
}

const authSlice = createSlice({
    name: 'authSlice',
    initialState,
    reducers: {
        setAuth: (state, action) => {
            state.username = action.payload.username;
            state.email = action.payload.email;
            state.uuid = action.payload.uuid;
            state.connected = action.payload.connected;
        }
    },
})

// dvalue from store in the slice file. For example: `useSelector((state) => state.counter.value)`
export const getAuth = state => state.persistedReducer.getAuth;

//export actions
export const { setAuth } = authSlice.actions;

//export reducer
export default authSlice;