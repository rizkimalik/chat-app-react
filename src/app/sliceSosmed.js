import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "./config";

export const getLoadConversation = createAsyncThunk(
    "sosialmedia/getLoadConversation",
    async ({ chat_id, customer_id }) => {
        const res = await axios.post(`${baseUrl}/omnichannel/conversation_chats`, { chat_id, customer_id });
        return res.data;
    }
)

const sliceSosmed = createSlice({
    name: "sosialmedia",
    initialState: {
        active_chat: {},
        conversations: [],
    },
    reducers: {
        setActiveChat: (state, action) => {
            state.active_chat = action.payload;
        },
    },
    extraReducers: {
        [getLoadConversation.fulfilled]: (state, action) => {
            state.conversations = action.payload
        },
    },
});
export const getActiveChat = state => state.persistedReducer.getActiveChat;

//export actions & reducer
export const {
    setActiveChat,
} = sliceSosmed.actions;

export default sliceSosmed;