import axios from "axios";
import { io } from "socket.io-client";

// const url = `${window.location.protocol}//${window.location.host}/ApiShopee`;
// const baseUrl = import.meta.env.REACT_APP_REST_API_URL;
const baseUrl = "http://localhost:3001";
const apiHeaders = {
    'Content-Type': 'application/json',
}
const socket = io(baseUrl);
const urlAttachment = baseUrl + '/attachment/chat';
const urlCall = "http://localhost:3001";

const axiosDefault = () => {
    axios.defaults.baseURL = baseUrl;
    axios.defaults.headers.post['Content-Type'] = 'application/json';
    axios.defaults.credentials = 'include';
    // const axiosDefault = (token) => {
    // axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
}

const httpClient = axios.create({
    baseURL: baseUrl,
    headers: apiHeaders,
});

const pcConfig = {
    "iceServers":
        [
            { "url": "stun:103.82.241.89:5555" },
            {
                "url": "turn:103.82.241.89:5555",
                "username": "usertest",
                "credential": "usertest123"
            }
        ]
}


export { socket, axiosDefault, baseUrl, apiHeaders, httpClient, pcConfig, urlAttachment, urlCall }