import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl } from "../config";


export const apiAuthLogin = createAsyncThunk(
    "auth/apiAuthLogin",
    async (data) => {
        const res = await axios.post(`${baseUrl}/omnichannel/join_chat`, data);
        return res.data;
    }
)


const sliceAuth = createSlice({
    name: "auth",
    initialState: {
        response: {},
    },
    extraReducers: {
        [apiAuthLogin.fulfilled]: (state, action) => {
            state.response = action.payload
        },
    },
});

export default sliceAuth;