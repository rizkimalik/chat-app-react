import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from "redux";

import auth from './sliceAuth';
import sosialmedia from './sliceSosmed';
import sliceAuth from './api/apiAuth';


const persistConfig = {
    key: 'auth',
    version: 1,
    storage
}

const reducer = combineReducers({
    getAuth: auth.reducer,
    getActiveChat: sosialmedia.reducer
})
const persistedReducer = persistReducer(persistConfig, reducer);

const rootReducer = {
    persistedReducer,
    auth: sliceAuth.reducer,
    sosialmedia: sosialmedia.reducer
}

export default rootReducer;
