import { Outlet, Navigate } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { getAuth } from '../app/sliceAuth';

const PublicRoute = () => {
    const auth = useSelector(getAuth);

    return (
        auth.email ? <Navigate to="/main" /> : <Outlet />
    )
}

export default PublicRoute