// import { lazy } from 'react';
// const Profile = lazy(() => import('../views/pages/Profile'))
// import Profile from '../views/pages/Main'
import Call from '../views/pages/Call';
import Main from '../views/pages/Main'

const routes = [
    {
        path: '/main', 
        element: <Main />,
    },{
        path: '/call', 
        element: <Call />,
    },

];

export default routes;