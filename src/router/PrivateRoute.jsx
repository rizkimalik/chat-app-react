import { Outlet, Navigate } from 'react-router-dom'
import { useSelector } from 'react-redux';
import { getAuth } from '../app/sliceAuth';

const PrivateRoute = () => {
    const auth = useSelector(getAuth);

    return (
        auth.email ? <Outlet /> : <Navigate to="/" />
    )
}

export default PrivateRoute