import React, { useEffect } from 'react'
import Modal from '../components/Modal'
import { AskPermission } from '../components/Notification';

function Master({ children }) {
    useEffect(() => {
        AskPermission();
    }, []);

    return (
        <div className="layout-wrapper d-lg-flex">
            <div className="w-100 overflow-hidden">
                <div className="user-chat-overlay" />
                <div className="chat-content d-lg-flex ">
                    <div className="w-100 overflow-hidden position-relative">
                        
                        {children}

                    </div>
                </div>
            </div>

            <Modal />
        </div >
    )
}

export default Master