import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import { socket, pcConfig } from '../../app/config';
import { getAuth } from '../../app/sliceAuth';
import { getActiveChat } from '../../app/sliceSosmed';
import Profile from '../components/Profile';

function Call() {
    const auth = useSelector(getAuth);
    const navigate = useNavigate();
    const { active_chat } = useSelector(getActiveChat);
    const [isCall, setIsCall] = useState(true);
    const [onMic, setOnMic] = useState(true);
    const [onCamera, setOnCamera] = useState(true);
    const [showProfile, setShowProfile] = useState(false);


    async function onSignOut() {
        localStorage.clear();
        navigate('/login');
        window.location.reload();
    }

    async function onOpenChat() {
        navigate('/main');
        window.location.reload();
    }

    useEffect(() => {
        const LoadExternalScript = () => {
            window.pcConfig = pcConfig;
            window.socket = socket;
            window.auth = auth;
            window.active = active_chat;
            const externalScript = document.createElement("script");
            externalScript.onerror = 'error';
            externalScript.id = "external";
            externalScript.async = true;
            externalScript.type = "text/javascript";
            externalScript.setAttribute("crossorigin", "anonymous");
            document.body.appendChild(externalScript);
            externalScript.src = `/assets/js/call_webrtc.js`;
        }
        LoadExternalScript();
    }, []);

    return (
        <>
            <div className="w-100 overflow-hidden">
                <ul className="list-inline user-chat-nav text-end" style={{ position: 'absolute', top: '10px', right: '10px', zIndex: '1' }}>
                    <li className="list-inline-item">
                        <button type="button" className="btn nav-btn" onClick={onOpenChat}>
                            <i className="bx bx-message-square" />
                        </button>
                    </li>
                    <li className="list-inline-item">
                        <div className="dropdown">
                            <button className="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="bx bx-dots-vertical-rounded" />
                            </button>
                            <div className="dropdown-menu dropdown-menu-end">
                                <Link to="#" className="dropdown-item d-flex justify-content-between align-items-center" onClick={() => setShowProfile(true)}>Profile <i className="bx bx-user text-muted" /></Link>
                                <Link to="#" className="dropdown-item d-flex justify-content-between align-items-center" onClick={onSignOut}>Sign Out <i className="bx bx-message-square-x text-muted" /></Link>
                            </div>
                        </div>
                    </li>
                </ul>

                <div className="chat-conversation d-flex justify-content-center align-items-center bottom-0 top-0">
                    <div style={{ position: 'absolute', top: '0', left: '0' }}>
                        <video name="localVideo" id="localVideo" autoPlay playsInline muted style={{ width: '100px' }} />
                    </div>
                    <video name="remoteVideo" id="remoteVideo" autoPlay playsInline muted className="h-100 top-0" />
                </div>
                <div className="position-absolute start-0 end-0 bottom-0">
                    <div className="text-center">
                        <div className="d-flex justify-content-center align-items-center text-center">
                            <div className="avatar-md h-auto">
                                <button type="button" onClick={(e) => setOnMic(false)} className={`btn btn-light avatar-sm rounded-circle ${onMic ? '' : 'hide'}`}>
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-microphone" />
                                    </span>
                                </button>
                                <button type="button" onClick={(e) => setOnMic(true)} className={`btn btn-light avatar-sm rounded-circle ${onMic ? 'hide' : ''}`}>
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-microphone-off" />
                                    </span>
                                </button>
                            </div>
                            <div className="avatar-md h-auto">
                                <button type="button" onClick={(e) => setOnCamera(false)} className={`btn btn-light avatar-sm rounded-circle ${onCamera ? '' : 'hide'}`}>
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-video" />
                                    </span>
                                </button>
                                <button type="button" onClick={(e) => setOnCamera(true)} className={`btn btn-light avatar-sm rounded-circle ${onCamera ? 'hide' : ''}`}>
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-video-off" />
                                    </span>
                                </button>
                            </div>
                        </div>

                        <div className="mt-4 py-4 bg-primary">
                            <button type="button" onClick={(e) => setIsCall(false)} className={`btn btn-success avatar-md call-close-btn rounded-circle mx-2 ${isCall ? '' : 'hide'}`} id='btn-call'>
                                <span className="avatar-title bg-transparent font-size-24">
                                    <i className="bx bx-phone-call" />
                                </span>
                            </button>

                            <button type="button" onClick={(e) => setIsCall(true)} className={`btn btn-danger avatar-md call-close-btn rounded-circle mx-2 ${isCall ? 'hide' : ''}`} id='btn-hangup'>
                                <span className="avatar-title bg-transparent font-size-24">
                                    <i className="bx bx-phone-off" />
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {/* if profile ccomponent */}
            {showProfile && <Profile auth={auth} setShowProfile={setShowProfile} />}
        </>
    )
}

export default Call