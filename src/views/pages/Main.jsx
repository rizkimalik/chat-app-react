import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import { socket, urlAttachment } from '../../app/config';
import Header from '../components/Header'
import Conversation from '../components/Conversation'
import FormSendMessage from '../components/FormSendMessage'
import Master from '../layouts/Master';
import { getAuth } from '../../app/sliceAuth';
import Datetime from '../components/Datetime';
import {
    setActiveChat,
    getActiveChat,
} from '../../app/sliceSosmed';
import Profile from '../components/Profile';
import { ShowNotification } from '../components/Notification';
import SocketIO from '../components/SocketIO';


function Main() {
    const dispatch = useDispatch();
    const auth = useSelector(getAuth);
    const [conversation, setConversation] = useState([]);
    const [message, setMessage] = useState('');
    const [attachment, setAttachment] = useState('');
    const [showProfile, setShowProfile] = useState(false);
    const [typing, setTyping] = useState(false);
    const [queing, setQueing] = useState('');
    const { conversations } = useSelector(state => state.sosialmedia);
    const { active_chat } = useSelector(getActiveChat);


    useEffect(() => {
        socket.on('send-message-agent', (res) => {
            //push from blending
            incomingMessage(res);
        });

        socket.on('return-message-agent', (res) => {
            incomingMessage(res);
        });

        socket.on('queing', (res) => {
            setQueing(res);
        });

        socket.on('return-typing', (res) => {
            setTyping(res.typing);
        });
    }, []);

    useEffect(() => {
        setConversation(conversations.data)
    }, [conversations]);

    function onKeyTyping() {
        let data = {
            uuid_customer: socket.id,
            uuid_agent: active_chat.uuid_agent,
            email: auth.email,
            agent_handle: active_chat.agent_handle,
            flag_to: 'customer',
            typing: true
        }
        socket.emit('typing', data);
    }

    function incomingMessage(res) {
        const { chat_id, user_id, customer_id, email, agent_handle, uuid_customer, uuid_agent } = res;
        setQueing('');
        dispatch(setActiveChat({ chat_id, user_id, customer_id, email, agent_handle, uuid_customer, uuid_agent })); //inital chat
        setConversation(
            conversation => [...conversation, res]
        );
        ShowNotification(res.name, res.message);
    }

    const sendMessage = (e) => {
        e.preventDefault();

        let message_type = 'text';
        let attachment_file = '';
        if (attachment) {
            let generate_filename = Date.now() + '.' + (attachment.name).split('.').pop();
            message_type = (attachment.type).split('/')[0] === 'image' ? 'image' : 'document';
            attachment_file = [{
                attachment: attachment,
                file_origin: attachment?.name,
                file_name: generate_filename,
                file_size: attachment?.size,
                file_url: attachment ? `${urlAttachment}/${generate_filename}` : '',
            }];
        }

        let data = {
            username: active_chat.agent_handle,
            chat_id: active_chat.chat_id,
            user_id: active_chat.user_id,
            customer_id: active_chat.customer_id,
            message: message,
            message_type: message_type,
            name: auth.username,
            email: auth.email,
            channel: 'Chat',
            flag_to: 'customer',
            agent_handle: active_chat.agent_handle,
            page_id: active_chat.page_id,
            date_create: Datetime(),
            uuid_customer: socket.id,
            uuid_agent: active_chat.uuid_agent,
            attachment: attachment_file,
            typing: false
        }

        if (message) {
            socket.emit('send-message-customer', data);
            setConversation(
                conversation => [...conversation, data]
            );
        }
        setMessage('');
        setAttachment('');
    }


    return (
        <Master>
            <SocketIO /> {/* Load Socket Module Global */}

            <div id="users-chat" className="position-relative">
                <Header
                    typing={typing}
                    queing={queing}
                    agent_handle={active_chat.agent_handle}
                    setShowProfile={setShowProfile}
                />
                <Conversation conversation={conversation} />
            </div>

            <FormSendMessage
                message={message}
                setMessage={setMessage}
                attachment={attachment}
                setAttachment={setAttachment}
                sendMessage={sendMessage}
                onKeyTyping={onKeyTyping}
            />

            {/* show profile component */}
            {
                showProfile &&
                <Profile auth={auth} setShowProfile={setShowProfile} />
            }
        </Master>
    )
}

export default Main