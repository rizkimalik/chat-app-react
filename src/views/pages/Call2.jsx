import React, { useState, useRef, useEffect } from 'react'
import { useSelector } from 'react-redux';
import { socket } from '../../app/config';
import { getAuth } from '../../app/sliceAuth';
import { getActiveChat } from '../../app/sliceSosmed';
import External from '../components/External';

function Call() {
    // const dispatch = useDispatch();
    const auth = useSelector(getAuth);
    const { active_chat } = useSelector(getActiveChat);

    const localVideo = useRef();
    const remoteVideo = useRef();
    const [isCall, setIsCall] = useState(true);
    const [peerConnection, setPeerConnection] = useState(new RTCPeerConnection());
    const [callInProgress, setCallInProgress] = useState(false);
    const [localStream, setLocalStream] = useState('');
    const [otherUser, setOtherUser] = useState('');
    const [remoteRTCMessage, setRemoteRTCMessage] = useState('');
    const [iceCandidatesFromCaller, setIceCandidatesFromCaller] = useState([]);


    // useEffect(() => {
    // function getMedia() {
    //     navigator.mediaDevices.getUserMedia({ video: true, audio: true })
    //         .then((stream) => {
    //             localVideo.current.srcObject = stream
    //         })
    // }
    // getMedia();
    // }, [])

    useEffect(() => {
        const fields = {
            flag_to: 'customer',
            username: auth.username,
            email: auth.email,
            uuid: socket.id,
            connected: socket.connected
        }
        if (!socket.connected) {
            socket.auth = fields;
            socket.connect();
        }
        console.log(socket.id, socket.connected)


        socket.on('connect', function () {
            fields.uuid = socket.id;
            fields.connected = socket.connected;
            socket.emit('reconnect', fields);

            socket.on('newCall', data => {
                // otherUser = data.caller;
                // remoteRTCMessage = data.rtcMessage;
                setOtherUser(data.caller)
                setRemoteRTCMessage(data.rtcMessage)
                alert('Incomming Call')
                //     .then((result) => {
                //             acceptCall();
                //             hangupCall();
                //         }
            })

            socket.on('callAnswered', data => {
                console.log(data.rtcMessage)
                // remoteRTCMessage = data.rtcMessage
                // setRemoteRTCMessage(data.rtcMessage)
                peerConnection.setRemoteDescription(new RTCSessionDescription(data.rtcMessage));
                // peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
                console.log("Call Started. They Answered");
                setCallInProgress(true)
            })

            socket.on("return-hangup", data => {
                console.log("Call hangup");
                hangupCall();
            });

            socket.on('ICEcandidate', data => {
                console.log("GOT ICE candidate");
                // let { label, candidate } = data.rtcMessage
                // let candidate2 = new RTCIceCandidate({
                //     sdpMLineIndex: label,
                //     candidate: candidate
                // });

                if (peerConnection) {
                    console.log("ICE candidate Added");
                    peerConnection.addIceCandidate(new RTCIceCandidate({
                        sdpMLineIndex: data.rtcMessage.label,
                        candidate: data.rtcMessage.candidate
                    }));
                } else {
                    console.log("ICE candidate Pushed");
                    // iceCandidatesFromCaller.push(candidate);
                    setIceCandidatesFromCaller(new RTCIceCandidate({
                        sdpMLineIndex: data.rtcMessage.label,
                        candidate: data.rtcMessage.candidate
                    }));
                }
            })
        });


    }, [auth.username]);

    useEffect(() => {
        window.onbeforeunload = function () {
            if (callInProgress) {
                stop();
            }
        };
        // window.addEventListener("beforeunload", onUnload);
    }, [callInProgress])


    async function startCall() {
        console.log(active_chat.agent_handle);
        setOtherUser(active_chat.agent_handle);
        await getMediaDevices()
            .then(bool => {
                processCall(active_chat.agent_handle)
            });
    }

    async function getMediaDevices() {
        return navigator.mediaDevices.getUserMedia({
            audio: true,
            video: true
        })
            .then(stream => {
                setLocalStream(stream);
                localVideo.current.srcObject = stream;
                setIsCall(false);
                setPeerConnection(new RTCPeerConnection());

                return createConnectionAndAddStream(stream)
            })
            .catch(function (e) {
                alert('getUserMedia() error: ' + e);
            });
    }

    async function acceptCall() {
        await getMediaDevices()
            .then(bool => {
                processAccept();
            })
    }

    async function hangupCall() {
        console.log('Hanging up.');
        setIsCall(true)
        socket.emit("hangup", {
            username: otherUser,
            rtcMessage: remoteRTCMessage
        });
        // if (callInProgress) {
        await stop();
        // }
    }

    function answerCall(data) {
        socket.emit("answerCall", data);
        setCallInProgress(true);
    }

    async function stop() {
        localStream.getTracks().forEach(track => track.stop());
        setCallInProgress(false);
        peerConnection.close();
        setPeerConnection(null);
        remoteVideo.current.srcObject = null;
        localVideo.current.srcObject = null;
        setOtherUser('')
    }

    async function processCall(userName) {
        peerConnection.createOffer((sessionDescription) => {
            peerConnection.setLocalDescription(sessionDescription);
            socket.emit("call", {
                username: userName,
                rtcMessage: sessionDescription
            });
            // sendCall
        },
            (error) => {
                console.log("Error");
            });
    }

    function processAccept() {
        peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
        peerConnection.createAnswer((sessionDescription) => {
            peerConnection.setLocalDescription(sessionDescription);
            if (iceCandidatesFromCaller.length > 0) {
                //I am having issues with call not being processed in real world (internet, not local)
                //so I will push iceCandidates I received after the call arrived, push it and, once we accept
                //add it as ice candidate
                //if the offer rtc message contains all thes ICE candidates we can ingore this.
                for (let i = 0; i < iceCandidatesFromCaller.length; i++) {
                    //
                    // let candidate = iceCandidatesFromCaller[i];
                    console.log("ICE candidate Added From queue");
                    try {
                        peerConnection.addIceCandidate(iceCandidatesFromCaller[i]).then(done => {
                            console.log(done);
                        }).catch(error => {
                            console.log(error);
                        })
                    } catch (error) {
                        console.log(error);
                    }
                }
                // iceCandidatesFromCaller = [];
                setIceCandidatesFromCaller([]);
                console.log("ICE candidate queue cleared");
            } else {
                console.log("NO Ice candidate in queue");
            }

            answerCall({
                caller: otherUser,
                rtcMessage: sessionDescription
            })

        }, (error) => {
            console.log("Error");
        })
    }



    //? Peer Connection
    function createPeerConnection() {
        try {
            // peerConnection = new RTCPeerConnection(pcConfig);
            // peerConnection = new RTCPeerConnection();
            // setPeerConnection(new RTCPeerConnection());
            peerConnection.onicecandidate = handleIceCandidate;
            peerConnection.onaddstream = handleRemoteStreamAdded;
            peerConnection.onremovestream = handleRemoteStreamRemoved;
            console.log('Created RTCPeerConnnection');
            return;
        } catch (e) {
            console.log('Failed to create PeerConnection, exception: ' + e.message);
            alert('Cannot create RTCPeerConnection object.');
            return;
        }
    }

    function createConnectionAndAddStream(stream) {
        createPeerConnection();
        // peerConnection.addStream(localStream);
        stream.getTracks().forEach((track) => peerConnection.addTrack(track, stream));

        return true;
    }

    function handleIceCandidate(event) {
        if (event.candidate) {
            console.log("Local ICE candidate");
            // console.log(event.candidate.candidate);
            sendICEcandidate({
                username: otherUser,
                rtcMessage: {
                    label: event.candidate.sdpMLineIndex,
                    id: event.candidate.sdpMid,
                    candidate: event.candidate.candidate
                }
            })
        } else {
            console.log('End of candidates.');
        }
    }

    function handleRemoteStreamAdded(event) {
        console.log('Remote stream added.');
        // remoteStream = event.stream;
        remoteVideo.current.srcObject = event.stream;
    }

    function handleRemoteStreamRemoved(event) {
        console.log('Remote stream removed. Event: ', event);
        remoteVideo.current.srcObject = null;
        localVideo.current.srcObject = null;
    }

    function sendICEcandidate(data) {
        //send only if we have caller, else no need to
        console.log("Send ICE candidate");
        socket.emit("ICEcandidate", data)
    }


    return (
        <>
            <div className="w-100 overflow-hidden">
                <div className="chat-conversation d-flex justify-content-center align-items-center bottom-0 top-0">
                    <div style={{ position: 'absolute', top: '0', left: '0' }}>
                        <video name="localVideo" autoPlay playsInline muted ref={localVideo} style={{ width: '100px' }} />
                    </div>
                    <video name="remoteVideo" id="remoteVideo" autoPlay playsInline muted ref={remoteVideo} className="h-100 top-0" />
                </div>
                <div className="position-absolute start-0 end-0 bottom-0">
                    <div className="text-center">
                        <div className="d-flex justify-content-center align-items-center text-center">
                            <div className="avatar-md h-auto">
                                <button type="button" className="btn btn-light avatar-sm rounded-circle" id="btn-voice">
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-microphone" />
                                    </span>
                                </button>
                            </div>
                            <div className="avatar-md h-auto">
                                <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                        <i className="bx bx-video" />
                                    </span>
                                </button>
                            </div>
                        </div>

                        <div className="mt-4 py-4 bg-primary">
                            {
                                isCall ? (
                                    <button type="button" onClick={startCall} className="btn btn-success avatar-md call-close-btn rounded-circle">
                                        <span className="avatar-title bg-transparent font-size-24">
                                            <i className="bx bx-phone-call" />
                                        </span>
                                    </button>
                                )
                                    : (
                                        <button type="button" onClick={hangupCall} className="btn btn-danger avatar-md call-close-btn rounded-circle">
                                            <span className="avatar-title bg-transparent font-size-24">
                                                <i className="bx bx-phone-off" />
                                            </span>
                                        </button>
                                    )
                            }
                        </div>
                    </div>
                </div>
            </div>
            <External />
        </>
    )
}

export default Call