import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import Master from '../layouts/Master'
import { setAuth } from '../../app/sliceAuth';
import { apiAuthLogin } from '../../app/api/apiAuth';
import { socket } from '../../app/config';
import {
    getLoadConversation,
    setActiveChat
} from '../../app/sliceSosmed';

function Login() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [fields, setFields] = useState({
        username: '',
        email: '',
    });


    const onHandleChange = (event) => {
        const name = event.target.name;
        setFields({
            ...fields,
            [name]: event.target.value
        });
    }

    const onAuthLogin = async (e) => {
        e.preventDefault();

        fields.uuid = socket.id;
        fields.connected = socket.connected;
        fields.flag_to = 'customer';
        const { payload } = await dispatch(apiAuthLogin(fields));

        if (payload.status === 200) {
            const data = payload.data;
            socket.auth = fields;
            socket.connect();
            socket.on('connect', function () {
                fields.uuid = socket.id;
                fields.connected = socket.connected;
                socket.emit('reconnect', fields);

                if (data) {
                    dispatch(setActiveChat(data))
                    dispatch(getLoadConversation({ chat_id: data.chat_id, customer_id: data.customer_id }))
                }
                dispatch(setAuth(fields)); //? set data local browser
                navigate('/main');
            });
        }
    }


    async function onOpenCall() {
        // window.open("http://localhost/mendawai/PORTSIP/portsip_webrtc_sdk/index.html", "webrtc", "width=800,height=600")
        window.open("https://app.mendawai.com/webrtc", "webrtc", "width=800,height=600")
    }

    return (
        <Master>
            <div className="bg">
                <div className="container-fluid p-0">
                    <div className="row g-0">
                        <div className="col-xl-12 col-lg-12">
                            <div className="authentication-page-content">
                                <div className="d-flex flex-column h-100 px-4 pt-4">
                                    <div className="row justify-content-center my-auto">
                                        <div className="col-sm-8 col-lg-6 col-xl-5 col-xxl-4">
                                            <div className="py-md-5 py-4">
                                                <div className="text-center mb-5">
                                                    <div className="mt-10">
                                                        <img src="assets/img/logo-hitam.png" style={{ height: 40 }} />
                                                    </div>
                                                    <p className="text-muted">Sign in continue to Chat.</p>
                                                </div>
                                                <form onSubmit={onAuthLogin}>
                                                    <div className="mb-3">
                                                        <label htmlFor="username" className="form-label">Full Name</label>
                                                        <input
                                                            type="text"
                                                            name="username"
                                                            value={fields.username}
                                                            className="form-control"
                                                            autoComplete="username"
                                                            placeholder="Enter Username"
                                                            required={true}
                                                            onChange={onHandleChange}
                                                            autoFocus={true}
                                                        />
                                                    </div>
                                                    <div className="mb-3">
                                                        <label htmlFor="email" className="form-label">Email</label>
                                                        <input
                                                            type="email"
                                                            name="email"
                                                            value={fields.email}
                                                            className="form-control"
                                                            autoComplete="email"
                                                            placeholder="Enter Email"
                                                            required={true}
                                                            onChange={onHandleChange}
                                                        />
                                                    </div>
                                                    <div className="form-check form-check-info font-size-16">
                                                        <input className="form-check-input" type="checkbox" id="remember-check" />
                                                        <label className="form-check-label font-size-14" htmlFor="remember-check">
                                                            Remember me
                                                        </label>
                                                    </div>
                                                    <div className="text-center mt-4">
                                                        <button className="btn btn-primary w-100" id="btn-login" type="submit">Log In</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-4"></div>
                                        <div className="col-lg-4">
                                            <button className="btn btn-danger w-100" onClick={() => onOpenCall()}>
                                                <i className="bx bxs-phone-call" /> Call Us
                                            </button>
                                        </div>
                                        <div className="col-lg-4"></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xl-12">
                                            <div className="text-center text-muted p-4">
                                                <p className="mb-0">©
                                                    Mendawai.
                                                    <i className="mdi mdi-heart text-danger" /> by Selindo Alpha
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Master>
    )
}

export default Login