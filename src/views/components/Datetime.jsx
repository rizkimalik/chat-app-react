function Datetime() {
    var e = 12 <= (new Date).getHours() ? "pm" : "am",
        t = 12 < (new Date).getHours() ? (new Date).getHours() % 12 : (new Date).getHours(),
        a = (new Date).getMinutes() < 10 ? "0" + (new Date).getMinutes() : (new Date).getMinutes();
    return t < 10 ? "0" + t + ":" + a + " " + e : t + ":" + a + " " + e
}

export default Datetime