import React from 'react'

function Attachment({ value }) {
    return (
        <div className="d-flex align-items-center">
            {
                value?.map((item, index) => {
                    return (
                        <div className="p-3 border-primary border rounded-3 m-2" key={index}>
                            <div className="d-flex align-items-center attached-file">
                                <div className="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                        <i className="bx bx-paperclip align-middle" />
                                    </div>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <div className="text-start">
                                        <h5 className="font-size-14 mb-1">{item.file_origin}</h5>
                                        <p className="text-muted text-truncate font-size-13 mb-0">{item.file_size / 1000} kb</p>
                                    </div>
                                </div>
                                <div className="flex-shrink-0 ms-4">
                                    <div className="d-flex gap-2 font-size-20 d-flex align-items-start">
                                        <div>
                                            <a href={item.file_url} target="_blank" rel="noopener noreferrer" className="text-muted">
                                                <i className="bx bxs-download" />
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    )
}

export default Attachment