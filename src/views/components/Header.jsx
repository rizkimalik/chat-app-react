import React from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { urlCall } from '../../app/config';
import AlertTopbar from './AlertTopbar';

function Header({ typing, agent_handle, queing, setShowProfile }) {
    const navigate = useNavigate();

    async function onSignOut() {
        localStorage.clear();
        navigate('/login');
        window.location.reload();
    }

    async function onOpenCall() {
        window.open(urlCall, "webrtc", "width=800,height=600")
        // navigate('/call');
        // window.location.reload();
    }

    return (
        <div className="p-3 p-lg-4 user-chat-topbar">
            <div className="row align-items-center">
                <div className="col-sm-4 col-8">
                    <div className="d-flex align-items-center">
                        <div className="flex-grow-1 overflow-hidden">
                            <div className="d-flex align-items-center">
                                <div className="flex-shrink-0 avatar-sm online align-self-center me-3 ms-0">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i className="bx bxs-message-square-dots"></i>
                                    </div>
                                    <span className="user-status"></span>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <h6 className="text-truncate mb-0 font-size-18">
                                        <a href="#" className="user-profile-show text-reset">{agent_handle ? agent_handle : 'waiting..'}</a>
                                    </h6>
                                    <p className="text-truncate text-muted mb-0"><small>{typing ? 'typing..' : 'online'}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-sm-8 col-4">
                    <ul className="list-inline user-chat-nav text-end mb-0">
                        <li className="list-inline-item">
                            <button type="button" className="btn nav-btn" onClick={onOpenCall}>
                                <i className="bx bxs-phone-call" />
                            </button>
                        </li>
                        {/* <li className="list-inline-item">
                            <button type="button" className="btn nav-btn" onClick={onOpenCall}>
                                <i className="bx bx-video" />
                            </button>
                        </li> */}
                        <li className="list-inline-item">
                            <div className="dropdown">
                                <button className="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i className="bx bx-dots-vertical-rounded" />
                                </button>
                                <div className="dropdown-menu dropdown-menu-end">
                                    <Link className="dropdown-item d-flex justify-content-between align-items-center user-profile-show" to="#" onClick={() => setShowProfile(true)}>Profile <i className="bx bx-user text-muted" /></Link>
                                    <Link className="dropdown-item d-flex justify-content-between align-items-center" to="#" onClick={onSignOut}>Sign Out <i className="bx bx-message-square-x text-muted" /></Link>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            {queing && <AlertTopbar data={queing} />}
        </div>
    )
}

export default Header