import React from 'react'
import Message from './Message'

function Conversation({ conversation }) {
    // console.log(conversation);
    return (
        <div className="chat-conversation p-3 p-lg-4" id="chat-conversation" data-simplebar>
            <ul className="list-unstyled chat-conversation-list" id="users-conversation">
                {
                    conversation?.map((value, index) => {
                        return <Message value={value} key={index} />
                    })
                }
            </ul>
        </div>
    )
}

export default Conversation