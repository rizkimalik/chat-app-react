import React from 'react'

function Profile({ auth, setShowProfile }) {
    return (
        <div className="user-profile-sidebar d-block">
            <div className="p-3 border-bottom">
                <div className="user-profile-img">
                    <div className="profile-img rounded" />
                    <div className="overlay-content rounded">
                        <div className="user-chat-nav p-2">
                            <div className="d-flex w-100">
                                <div className="flex-grow-1">
                                    <button type="button" className="btn nav-btn text-white user-profile-show d-none d-lg-block" onClick={e => setShowProfile(false)}>
                                        <i className="bx bx-x" />
                                    </button>
                                    <button type="button" className="btn nav-btn text-white user-profile-show d-block d-lg-none" onClick={e => setShowProfile(false)}>
                                        <i className="bx bx-x" />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="mt-auto p-3">
                            <h5 className="user-name mb-1 text-truncate">{auth.username}</h5>
                            <p className="font-size-14 text-truncate mb-0">
                                <i className="bx bxs-circle font-size-10 text-success me-1 ms-0" />
                                {auth.connected ? 'online' : 'disconnect'}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            {/* End profile user */}
            {/* Start user-profile-desc */}
            <div className="p-4 user-profile-desc" data-simplebar>
                <div className="pb-2">
                    <h5 className="font-size-11 text-uppercase mb-2">Info :</h5>
                    <div>
                        <div className="d-flex align-items-end">
                            <div className="flex-grow-1">
                                <p className="text-muted font-size-14 mb-1">Name</p>
                            </div>
                            <div className="flex-shrink-0">
                                <button type="button" className="btn btn-sm btn-soft-primary">Edit</button>
                            </div>
                        </div>
                        <h5 className="font-size-14 text-truncate">{auth.username}</h5>
                    </div>
                    <div className="mt-4">
                        <p className="text-muted font-size-14 mb-1">Email</p>
                        <h5 className="font-size-14">{auth.email}</h5>
                    </div>
                    <div className="mt-4">
                        <p className="text-muted font-size-14 mb-1">Phone</p>
                        <h5 className="font-size-14 mb-0">08583493843</h5>
                    </div>
                </div>

                <hr className="my-4" />
                <div>
                    <div>
                        <h5 className="font-size-11 text-muted text-uppercase mb-3">Attached Files</h5>
                    </div>
                    <div>
                        <div className="card p-2 border mb-2">
                            <div className="d-flex align-items-center">
                                <div className="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i className="bx bx-file" />
                                    </div>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <h5 className="font-size-14 text-truncate mb-1">design-phase-1-approved.pdf</h5>
                                    <p className="text-muted font-size-13 mb-0">12.5 MB</p>
                                </div>
                                <div className="flex-shrink-0 ms-3">
                                    <div className="d-flex gap-2">
                                        <div>
                                            <a href="#" className="text-muted px-1">
                                                <i className="bx bxs-download" />
                                            </a>
                                        </div>
                                        <div className="dropdown">
                                            <a className="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="bx bx-dots-horizontal-rounded" />
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-end">
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i className="bx bx-share-alt ms-2 text-muted" /></a>
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i className="bx bx-bookmarks text-muted ms-2" /></a>
                                                <div className="dropdown-divider" />
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card p-2 border mb-2">
                            <div className="d-flex align-items-center">
                                <div className="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i className="bx bx-image" />
                                    </div>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <h5 className="font-size-14 text-truncate mb-1">Image-1.jpg</h5>
                                    <p className="text-muted font-size-13 mb-0">4.2 MB</p>
                                </div>
                                <div className="flex-shrink-0 ms-3">
                                    <div className="d-flex gap-2">
                                        <div>
                                            <a href="#" className="text-muted px-1">
                                                <i className="bx bxs-download" />
                                            </a>
                                        </div>
                                        <div className="dropdown">
                                            <a className="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="bx bx-dots-horizontal-rounded" />
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-end">
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i className="bx bx-share-alt ms-2 text-muted" /></a>
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i className="bx bx-bookmarks text-muted ms-2" /></a>
                                                <div className="dropdown-divider" />
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card p-2 border mb-2">
                            <div className="d-flex align-items-center">
                                <div className="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i className="bx bx-image" />
                                    </div>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <h5 className="font-size-14 text-truncate mb-1">Image-2.jpg</h5>
                                    <p className="text-muted font-size-13 mb-0">3.1 MB</p>
                                </div>
                                <div className="flex-shrink-0 ms-3">
                                    <div className="d-flex gap-2">
                                        <div>
                                            <a href="#" className="text-muted px-1">
                                                <i className="bx bxs-download" />
                                            </a>
                                        </div>
                                        <div className="dropdown">
                                            <a className="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="bx bx-dots-horizontal-rounded" />
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-end">
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i className="bx bx-share-alt ms-2 text-muted" /></a>
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i className="bx bx-bookmarks text-muted ms-2" /></a>
                                                <div className="dropdown-divider" />
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="card p-2 border mb-2">
                            <div className="d-flex align-items-center">
                                <div className="flex-shrink-0 avatar-xs ms-1 me-3">
                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                        <i className="bx bx-file" />
                                    </div>
                                </div>
                                <div className="flex-grow-1 overflow-hidden">
                                    <h5 className="font-size-14 text-truncate mb-1">Landing-A.zip</h5>
                                    <p className="text-muted font-size-13 mb-0">6.7 MB</p>
                                </div>
                                <div className="flex-shrink-0 ms-3">
                                    <div className="d-flex gap-2">
                                        <div>
                                            <a href="#" className="text-muted px-1">
                                                <i className="bx bxs-download" />
                                            </a>
                                        </div>
                                        <div className="dropdown">
                                            <a className="dropdown-toggle text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="bx bx-dots-horizontal-rounded" />
                                            </a>
                                            <div className="dropdown-menu dropdown-menu-end">
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Share <i className="bx bx-share-alt ms-2 text-muted" /></a>
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i className="bx bx-bookmarks text-muted ms-2" /></a>
                                                <div className="dropdown-divider" />
                                                <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* end user-profile-desc */}
        </div>
    )
}

export default Profile