import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getActiveChat } from '../../app/sliceSosmed';

function FormSendMessage({ setMessage, sendMessage, message, onKeyTyping, attachment, setAttachment }) {
    const inputFileRef = useRef(null);
    const { active_chat } = useSelector(getActiveChat);
    const [disable, setDisable] = useState(true);


    useEffect(() => {
        if (active_chat.agent_handle) {
            setDisable(false);
        }
    }, [active_chat]);

    const onFileChange = (e) => {
        console.log(e.target.files[0]);
        if (e.target.files[0].size > 3000000) {
            alert('Max size file 3mb.');
        } else {
            setAttachment(e.target.files[0]);
        }
        e.target.value = "";
    }

    const onFileClose = (e) => {
        setAttachment('')
        e.target.value = "";
    }

    return (
        <div className="position-relative">
            <div className="chat-input-section p-3 p-lg-4">
                <form id="chatinput-form" encType="multipart/form-data">
                    <div className="row g-0 align-items-center">
                        <div className="col-auto">
                            <div className="chat-input-links me-md-2">
                                <div className="links-list-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="File Document">
                                    <input type="file" ref={inputFileRef} onChange={onFileChange} accept="image/*, .doc, .pdf, .xls, .xlsx" className="hide" />
                                    <button type="button" onClick={() => inputFileRef.current.click()} className="btn btn-link text-decoration-none btn-lg waves-effect" aria-expanded="false" disabled={disable}>
                                        <i className="bx bx-paperclip align-middle" />
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col">
                            <div className="position-relative">
                                <div className="chat-input-feedback">
                                    Please Enter a Message
                                </div>
                                <input
                                    type="text"
                                    className="form-control form-control-lg chat-input"
                                    autoComplete="off"
                                    id="chat-input"
                                    placeholder="Type your message..."
                                    value={message}
                                    onChange={(e) => setMessage(e.target.value)}
                                    onKeyPress={e => e.key === 'Enter' ? sendMessage(e) : onKeyTyping()}
                                    autoFocus={true}
                                    required={true}
                                    disabled={disable}
                                />
                            </div>
                        </div>
                        <div className="col-auto">
                            <div className="chat-input-links ms-2 gap-md-1">
                                <div className="links-list-item">
                                    <button type="button" className="btn btn-primary btn-lg chat-send waves-effect waves-light" onClick={e => sendMessage(e)} disabled={disable}>
                                        <i className="bx bxs-send align-middle" id="submit-btn" />
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                {
                    // show attachment file
                    attachment &&
                    <div className="chat-input-collapse chat-input-collapse1 collapse show" id="chatinputmorecollapse">
                        <div className="card mb-0">
                            <div className="card-body py-2">
                                <div className="d-flex align-items-center attached-file">
                                    <div className="flex-shrink-0 avatar-sm me-3 ms-0 attached-file-avatar">
                                        <div className="avatar-title bg-soft-primary text-primary rounded-circle font-size-20">
                                            <i className="bx bxs-file-image" /> </div>
                                    </div>
                                    <div className="flex-grow-1 overflow-hidden">
                                        <div className="text-start">
                                            <h5 className="font-size-14 mb-1">{attachment?.name}</h5>
                                            <p className="text-muted text-truncate font-size-13 mb-0">{attachment?.size / 1000} kb</p>
                                        </div>
                                    </div>
                                    <div className="flex-shrink-0 ms-4">
                                        <div className="d-flex gap-2 font-size-20 d-flex align-items-start">
                                            <Link to="#" onClick={onFileClose} className="text-muted">
                                                <i className="bx bx-x-circle" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        </div>
    )
}

export default FormSendMessage