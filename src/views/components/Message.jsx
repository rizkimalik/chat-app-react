import React from 'react'
import Attachment from './Attachment'
import Tooltip from './Tooltip'

function Message({ value }) {
    if (value.status_chat === 'waiting') {
        return (
            <li className="chat-list left">
                <div className="conversation-list">
                    <div className="chat-avatar avatar-sm">
                        <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                            <i className="bx bxs-bot" />
                        </div>
                    </div>
                    <div className="user-chat-content">
                        <div className="ctext-wrap">
                            <div className="ctext-wrap-content">
                                <p className="mb-0 ctext-content">{value.message}</p>
                            </div>
                            <Tooltip />
                        </div>
                        <div className="conversation-name">
                            <small className="text-muted time">{value.date_create}</small>
                            <span className="text-success check-message-icon"><i className="bx bx-check-double" /></span>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
    else if (value.flag_to === 'customer') {
        return (
            <li className="chat-list right">
                <div className="conversation-list">
                    <div className="user-chat-content">
                        <div className="ctext-wrap">
                            <div className="ctext-wrap-content">
                                {value.attachment && <Attachment value={value.attachment} />}
                                <p className="mb-0 ctext-content">{value.message}</p>
                            </div>
                            <Tooltip />
                        </div>
                        <div className="conversation-name">
                            <small className="text-muted time">{value.date_create}</small>
                            <span className="text-success check-message-icon"><i className="bx bx-check-double" /></span>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
    else if (value.flag_to === 'agent') {
        return (
            <li className="chat-list left">
                <div className="conversation-list">
                    <div className="chat-avatar avatar-sm">
                        <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                            <i className="bx bxs-message-square-detail" />
                        </div>
                    </div>
                    <div className="user-chat-content">
                        <div className="ctext-wrap">
                            <div className="ctext-wrap-content">
                                {value.attachment && <Attachment value={value.attachment} />}
                                <p className="mb-0 ctext-content">{value.message}</p>
                            </div>
                            <Tooltip />
                        </div>
                        <div className="conversation-name">
                            <small className="text-muted time">{value.date_create}</small>
                            <span className="text-success check-message-icon"><i className="bx bx-check-double" /></span>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

export default Message