import React, { useEffect } from 'react'

function External() {
    useEffect(() => {
        const LoadExternalScript = () => {
            const externalScript = document.createElement("script");
            externalScript.onerror = 'error';
            externalScript.id = "external";
            externalScript.async = true;
            externalScript.type = "text/javascript";
            externalScript.setAttribute("crossorigin", "anonymous");
            document.body.appendChild(externalScript);
            externalScript.src = `/assets/js/call_webrtc.js`;
        };
        LoadExternalScript();
    }, []);

    return (
        <></>
    )
}

export default External