import React from 'react'

function Modal() {
    return (
        <>
            <div>
                {/* Start Add contact Modal */}
                <div className="modal fade" id="addContact-exampleModal" tabIndex={-1} role="dialog" aria-labelledby="addContact-exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div className="modal-content modal-header-colored shadow-lg border-0">
                            <div className="modal-header">
                                <h5 className="modal-title text-white font-size-16" id="addContact-exampleModalLabel">Create Contact
                                </h5>
                                <button type="button" className="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div className="modal-body p-4">
                                <form>
                                    <div className="mb-3">
                                        <label htmlFor="addcontactemail-input" className="form-label">Email</label>
                                        <input type="email" className="form-control" id="addcontactemail-input" placeholder="Enter Email" />
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="addcontactname-input" className="form-label">Name</label>
                                        <input type="text" className="form-control" id="addcontactname-input" placeholder="Enter Name" />
                                    </div>
                                    <div className="mb-0">
                                        <label htmlFor="addcontact-invitemessage-input" className="form-label">Invatation
                                            Message</label>
                                        <textarea className="form-control" id="addcontact-invitemessage-input" rows={3} placeholder="Enter Message" defaultValue={""} />
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-link" data-bs-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Invite</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Add contact Modal */}
                {/* audiocall Modal */}
                <div className="modal fade audiocallModal" tabIndex={-1} aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content shadow-lg border-0">
                            <div className="modal-body p-0">
                                <div className="text-center p-4 pb-0">
                                    <div className="avatar-xl mx-auto mb-4">
                                        <img src="https://themesbrand.com/doot/layouts/assets/images/users/avatar-2.jpg" className="img-thumbnail rounded-circle" />
                                    </div>
                                    <div className="d-flex justify-content-center align-items-center mt-4">
                                        <div className="avatar-md h-auto">
                                            <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                <span className="avatar-title bg-transparent text-muted font-size-20">
                                                    <i className="bx bx-microphone-off" />
                                                </span>
                                            </button>
                                            <h5 className="font-size-11 text-uppercase text-muted mt-2">Mute</h5>
                                        </div>
                                        <div className="avatar-md h-auto">
                                            <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                <span className="avatar-title bg-transparent text-muted font-size-20">
                                                    <i className="bx bx-volume-full" />
                                                </span>
                                            </button>
                                            <h5 className="font-size-11 text-uppercase text-muted mt-2">Speaker</h5>
                                        </div>
                                        <div className="avatar-md h-auto">
                                            <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                <span className="avatar-title bg-transparent text-muted font-size-20">
                                                    <i className="bx bx-user-plus" />
                                                </span>
                                            </button>
                                            <h5 className="font-size-11 text-uppercase text-muted mt-2">Add New</h5>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        <button type="button" className="btn btn-danger avatar-md call-close-btn rounded-circle" data-bs-dismiss="modal">
                                            <span className="avatar-title bg-transparent font-size-24">
                                                <i className="mdi mdi-phone-hangup" />
                                            </span>
                                        </button>
                                    </div>
                                </div>
                                <div className="p-4 bg-soft-primary mt-n4">
                                    <div className="mt-4 text-center">
                                        <h5 className="font-size-18 mb-0 text-truncate">Bella Cote</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* audiocall Modal */}
                {/* videocall Modal */}
                <div className="modal fade videocallModal" tabIndex={-1} aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content shadow-lg border-0">
                            <div className="modal-body p-0">
                                <img src="https://themesbrand.com/doot/layouts/assets/images/users/avatar-2.jpg" className="videocallModal-bg" />
                                <div className="position-absolute start-0 end-0 bottom-0">
                                    <div className="text-center">
                                        <div className="d-flex justify-content-center align-items-center text-center">
                                            <div className="avatar-md h-auto">
                                                <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                                        <i className="bx bx-microphone-off" />
                                                    </span>
                                                </button>
                                            </div>
                                            <div className="avatar-md h-auto">
                                                <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                                        <i className="bx bx-volume-full" />
                                                    </span>
                                                </button>
                                            </div>
                                            <div className="avatar-md h-auto">
                                                <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                                        <i className="bx bx-video-off" />
                                                    </span>
                                                </button>
                                            </div>
                                            <div className="avatar-md h-auto">
                                                <button type="button" className="btn btn-light avatar-sm rounded-circle">
                                                    <span className="avatar-title bg-transparent text-muted font-size-20">
                                                        <i className="bx bx-refresh" />
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <div className="mt-4">
                                            <button type="button" className="btn btn-danger avatar-md call-close-btn rounded-circle" data-bs-dismiss="modal">
                                                <span className="avatar-title bg-transparent font-size-24">
                                                    <i className="mdi mdi-phone-hangup" />
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                    <div className="p-4 bg-primary mt-n4">
                                        <div className="text-white mt-4 text-center">
                                            <h5 className="font-size-18 text-truncate mb-0 text-white">Bella Cote</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end modal */}
                {/* Start Add pinned tab Modal */}
                <div className="modal fade pinnedtabModal" tabIndex={-1} role="dialog" aria-labelledby="pinnedtabModalLabel" aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div className="modal-content modal-header-colored shadow-lg border-0">
                            <div className="modal-header">
                                <h5 className="modal-title text-white font-size-16" id="pinnedtabModalLabel">Bookmark</h5>
                                <button type="button" className="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close">
                                </button>
                            </div>
                            <div className="modal-body p-4">
                                <div className="d-flex align-items-center mb-3">
                                    <div className="flex-grow-1">
                                        <div>
                                            <h5 className="font-size-16 mb-0">10 Pinned tabs</h5>
                                        </div>
                                    </div>
                                    <div className="flex-shrink-0">
                                        <div>
                                            <button type="button" className="btn btn-sm btn-soft-primary"><i className="bx bx-plus" />
                                                Pin</button>
                                        </div>
                                    </div>
                                </div>
                                <div className="chat-bookmark-list mx-n4" data-simplebar style={{ maxHeight: 299 }}>
                                    <ul className="list-unstyled chat-list">
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-file" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">design-phase-1-approved.pdf</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">12.5 MB</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-pin" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">Bg
                                                        Pattern</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">https://bgpattern.com/</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-image" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">Image-001.jpg</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">4.2 MB</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-pin" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">Images</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">https://images123.com/</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-pin" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">Bg
                                                        Gradient</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">https://bggradient.com/</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-image" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">Image-012.jpg</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">3.1 MB</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="d-flex align-items-center">
                                                <div className="flex-shrink-0 avatar-xs me-3">
                                                    <div className="avatar-title bg-soft-primary text-primary rounded-circle">
                                                        <i className="bx bx-file" />
                                                    </div>
                                                </div>
                                                <div className="flex-grow-1 overflow-hidden">
                                                    <h5 className="font-size-14 text-truncate mb-1"><a href="#" className="p-0">analytics dashboard.zip</a></h5>
                                                    <p className="text-muted font-size-13 mb-0">6.7 MB</p>
                                                </div>
                                                <div className="flex-shrink-0 ms-3">
                                                    <div className="dropdown">
                                                        <a className="dropdown-toggle font-size-18 text-muted px-1" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i className="bx bx-dots-horizontal-rounded" />
                                                        </a>
                                                        <div className="dropdown-menu dropdown-menu-end">
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Open <i className="bx bx-folder-open ms-2 text-muted" /></a>
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Edit <i className="bx bx-pencil ms-2 text-muted" /></a>
                                                            <div className="dropdown-divider" />
                                                            <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Delete <i className="bx bx-trash ms-2 text-muted" /></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Add pinned tab Modal */}
            </div>



        </>
    )
}

export default Modal