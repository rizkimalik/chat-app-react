import React from 'react'

function Tooltip() {
    return (
        <div className="dropdown align-self-start message-box-drop">
            <a className="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i className="bx bx-dots-vertical-rounded align-middle" /> </a>
            <div className="dropdown-menu"> <a className="dropdown-item d-flex align-items-center justify-content-between reply-message" href="#" data-bs-toggle="collapse" data-bs-target=".replyCollapse">Reply <i className="bx bx-share ms-2 text-muted" /></a> <a className="dropdown-item d-flex align-items-center justify-content-between" href="#" data-bs-toggle="modal" data-bs-target=".forwardModal">Forward <i className="bx bx-share-alt ms-2 text-muted" /></a> <a className="dropdown-item d-flex align-items-center justify-content-between copy-message" href="#">Copy <i className="bx bx-copy text-muted ms-2" /></a> <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Bookmark <i className="bx bx-bookmarks text-muted ms-2" /></a> <a className="dropdown-item d-flex align-items-center justify-content-between" href="#">Mark as Unread <i className="bx bx-message-error text-muted ms-2" /></a> <a className="dropdown-item d-flex align-items-center justify-content-between delete-item" href="#">Delete <i className="bx bx-trash text-muted ms-2" /></a> </div>
        </div>
    )
}

export default Tooltip