import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { apiAuthLogin } from '../../app/api/apiAuth';

import { socket } from '../../app/config';
import { getAuth, setAuth } from '../../app/sliceAuth';
import { getLoadConversation, setActiveChat } from '../../app/sliceSosmed';

const SocketIO = () => {
    const dispatch = useDispatch();
    const auth = useSelector(getAuth);

    useEffect(async () => {
        if (!socket.connected) {
            const fields = {
                flag_to: 'customer',
                username: auth.username,
                email: auth.email,
                uuid: socket.id,
                connected: socket.connected
            }

            const { payload } = await dispatch(apiAuthLogin(fields));

            if (payload.status === 200) {
                const data = payload.data;

                socket.auth = fields;
                socket.connect();
                socket.on('connect', function () {
                    fields.uuid = socket.id;
                    fields.connected = socket.connected;
                    
                    if (data) {
                        // fields.agent_handle = data.agent_handle;
                        // socket.emit('reconnect', fields);
                        dispatch(setActiveChat(data))
                        dispatch(getLoadConversation({ chat_id: data.chat_id, customer_id: data.customer_id }))
                    }
                    dispatch(setAuth(fields)); //? set data local browser
                });
            }
        }
    }, [dispatch, auth.email])

    // useEffect(() => {
    //     const soundMessage = new Audio('assets/sound/MESSAGE.mp3');

    //     socket.on('return-message-agent', async (res) => {
    //         let { message, name } = res;
    //         console.log(res);
    //         // soundMessage.loop = true;
    //         soundMessage.play();
    //         await ShowNotification(name, message);
    //     });
    // }, [])

    return <></>;
}

export default SocketIO
