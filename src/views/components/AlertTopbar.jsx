import React from 'react'

function AlertTopbar({data}) {
    return (
        <div className="alert alert-warning alert-dismissible topbar-bookmark fade show p-1 px-3 px-lg-4 pe-lg-5 pe-5" role="alert">
            <div className="d-flex align-items-start bookmark-tabs">
                <div className="tab-list-link">
                    <a href="#" className="tab-links" data-bs-toggle="modal" data-bs-target=".pinnedtabModal">
                        <i className="ri-pushpin-fill align-middle me-1" /> Total {data.total_queing} queing</a>
                </div>
                <div>
                    <a href="#" className="tab-links border-0 px-3" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="bottom" title="Add Bookmark">
                        <i className="ri-add-fill align-middle" /> {data.message}</a>
                </div>
            </div>
            <button type="button" className="btn-close" data-bs-dismiss="alert" aria-label="Close" />
        </div>
    )
}

export default AlertTopbar