const localVideo = document.getElementById('localVideo');
const remoteVideo = document.getElementById('remoteVideo');
// const btnMic = document.getElementById('btn-mic');
// const btnCamera = document.getElementById('btn-camera');
const btnCall = document.getElementById('btn-call');
const btnHangup = document.getElementById('btn-hangup');

let otherUser;
let remoteRTCMessage;
let iceCandidatesFromCaller = [];
let peerConnection;
let remoteStream;
let localStream;
let callInProgress = false;

btnCall.addEventListener('click', call);
if (btnHangup) {
    btnHangup.addEventListener('click', hangup);
}

const fields = {
    flag_to: 'customer',
    username: auth.username,
    email: auth.email,
    uuid: socket.id,
    connected: socket.connected
}
if (!socket.connected) {
    socket.auth = fields;
    socket.connect();
}
socket.on('connect', function () {
    fields.uuid = socket.id;
    fields.connected = socket.connected;
    socket.emit('reconnect', fields);
    localStorage.removeItem('ActiveCall'); //clear
    console.log(socket);

    socket.on('newCall', data => {
        //when other called you
        otherUser = data.caller;
        remoteRTCMessage = data.rtcMessage;
        localStorage.setItem('ActiveCall', JSON.stringify(data));

        let incommingCall = confirm(`Incomming Call from : ${data.caller}`);
        if (incommingCall) {
            answer();
        } else {
            hangup();
        }
    })

    socket.on('callAnswered', data => {
        //when other accept our call
        remoteRTCMessage = data.rtcMessage
        peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
        console.log("Call Started. They Answered");
        callProgress()
    })

    socket.on("return-hangup", data => {
        console.log("Call hangup");
        hangup();
    });

    socket.on('ICEcandidate', data => {
        console.log("GOT ICE candidate");
        let message = data.rtcMessage
        let candidate = new RTCIceCandidate({
            sdpMLineIndex: message.label,
            candidate: message.candidate
        });

        if (peerConnection) {
            console.log("ICE candidate Added");
            peerConnection.addIceCandidate(candidate);
        } else {
            console.log("ICE candidate Pushed");
            iceCandidatesFromCaller.push(candidate);
        }
    })
});



async function call() {
    // console.log(active)
    if (active) {
        let userToCall = active.agent_handle;
        otherUser = userToCall;
        await beReady()
            .then(bool => {
                console.log(`call to ${userToCall}`)
                processCall(userToCall)
            })
    } else {
        alert('Call Failed, callee name not found.')
    }
}

//event from html
function answer() {
    btnCall.classList.add('hide');
    btnHangup.classList.remove('hide');
    //do the event firing
    beReady()
        .then(bool => {
            processAccept();
        })
}


function sendCall(data) {
    //to send a call
    console.log("Send Call");
    socket.emit("call", data);
}


function answerCall(data) {
    //to answer a call
    socket.emit("answerCall", data);
    callProgress();
}

function sendICEcandidate(data) {
    //send only if we have caller, else no need to
    console.log("Send ICE candidate");
    socket.emit("ICEcandidate", data)
}

async function beReady() {
    return await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
    })
        .then(stream => {
            localStream = stream;
            localVideo.srcObject = stream;

            return createConnectionAndAddStream()
        })
        .catch(function (e) {
            alert('getUserMedia() error: ' + e.name);
        });
}

function createConnectionAndAddStream() {
    createPeerConnection();
    peerConnection.addStream(localStream);
    return true;
}

function processCall(userName) {
    peerConnection.createOffer((sessionDescription) => {
        peerConnection.setLocalDescription(sessionDescription);
        sendCall({
            customer_id: active.customer_id,
            agent: active.agent_handle,
            email: active.email,
            username: userName,
            rtcMessage: sessionDescription
        })
    }, (error) => {
        console.log("Error");
    });
}

function processAccept() {
    peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
    peerConnection.createAnswer((sessionDescription) => {
        peerConnection.setLocalDescription(sessionDescription);

        if (iceCandidatesFromCaller.length > 0) {
            //I am having issues with call not being processed in real world (internet, not local)
            //so I will push iceCandidates I received after the call arrived, push it and, once we accept
            //add it as ice candidate
            //if the offer rtc message contains all thes ICE candidates we can ingore this.
            for (let i = 0; i < iceCandidatesFromCaller.length; i++) {
                //
                let candidate = iceCandidatesFromCaller[i];
                console.log("ICE candidate Added From queue");
                try {
                    peerConnection.addIceCandidate(candidate).then(done => {
                        console.log(done);
                    }).catch(error => {
                        console.log(error);
                    })
                } catch (error) {
                    console.log(error);
                }
            }
            iceCandidatesFromCaller = [];
            console.log("ICE candidate queue cleared");
        } else {
            console.log("NO Ice candidate in queue");
        }

        answerCall({
            caller: otherUser,
            rtcMessage: sessionDescription
        })

    }, (error) => {
        console.log("Error");
    })
}

///////////////////// Peer connection ////////////////////////////////////

function createPeerConnection() {
    try {
        // peerConnection = new RTCPeerConnection(pcConfig);
        peerConnection = new RTCPeerConnection();
        peerConnection.onicecandidate = handleIceCandidate;
        peerConnection.onaddstream = handleRemoteStreamAdded;
        peerConnection.onremovestream = handleRemoteStreamRemoved;
        console.log('Created RTCPeerConnnection');
        return;
    } catch (e) {
        console.log('Failed to create PeerConnection, exception: ' + e.message);
        alert('Cannot create RTCPeerConnection object.');
        return;
    }
}

function handleIceCandidate(event) {
    // console.log('icecandidate event: ', event);
    if (event.candidate) {
        console.log("Local ICE candidate");
        sendICEcandidate({
            username: otherUser,
            rtcMessage: {
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
            }
        })
    } else {
        console.log('End of candidates.');
    }
}

function handleRemoteStreamAdded(event) {
    console.log('Remote stream added.');
    remoteStream = event.stream;
    remoteVideo.srcObject = remoteStream;
}

function handleRemoteStreamRemoved(event) {
    console.log('Remote stream removed. Event: ', event);
    remoteVideo.srcObject = null;
    localVideo.srcObject = null;
}

async function stop() {
    localStream.getTracks().forEach(track => track.stop());
    callInProgress = false;
    if (peerConnection) {
        peerConnection.close();
    }
    peerConnection = null;
    remoteVideo.srcObject = null;
    localVideo.srcObject = null;
    otherUser = null;
    btnCall.classList.remove('hide');
    btnHangup.classList.add('hide');
}

async function hangup() {
    console.log('Hanging up.');
    const data = JSON.parse(localStorage.getItem('ActiveCall'));

    socket.emit("hangup", {
        call_id: data?.call_id,
        customer_id: data?.customer_id,
        username: otherUser,
        rtcMessage: remoteRTCMessage
    });
    await stop();
}

function callProgress() {
    callInProgress = true;
}

const onMic = async (e) => {
    console.log('mic')
}
const onCamera = async (e) => {
    console.log('camera')
}

async function isPlayVideo(value) {
    if (value) {
        const stream = await navigator.mediaDevices.getUserMedia({ video: true });
        localVideo.srcObject = stream;
        localStream = stream;
    }
    else {
        localStream.getTracks().forEach(function (track) {
            if (track.readyState == 'live' && track.kind === 'video') {
                track.stop();
            }
        });
        localVideo.srcObject = null;
    }
}